﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppUdpClient
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            for (int i = 0; i < 500; i++)
            {
                var ms = DateTime.Now.Millisecond;
                log.InfoFormat(
                    "{5}.{2}.{3}.{4} /api/v3/necklacesetting {1} 200 {0}",
                    Guid.NewGuid(), ms, ms % 255, ms % 255, ms % 255, ms % 255);
                Thread.Sleep(10);
            }

            //var client = new UdpClient();
            //IPEndPoint ep = new IPEndPoint(IPAddress.Parse("192.168.1.118"), 10000); // endpoint where server is listening
            //client.Connect(ep);

            
            //for (int i = 0; i < 100; i++)
            //{
            //    byte[] data = Encoding.ASCII.GetBytes("Hello World From C# " + (i+1));
            //    // send data
            //    client.Send(data, data.Length);
            //}

            //// then receive data
            //var receivedData = client.Receive(ref ep);

            //Console.Write("receive data from " + ep.ToString());

            Console.Read();
        }
    }
}
