﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net.Config;

namespace ConsoleApplication1
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            log.Info("Server Start");

            new SocketServer().Run();

            Console.ReadKey();
            Console.WriteLine("press any key to exit....");
            Console.ReadKey();
        }
    }

    interface IRun
    {
        void Run();
    }
    class SocketServer : IRun
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly int Port = 12000;

        private Socket _serverSocket;
        private Thread _listenThread;

        private Thread _sendThread;

        private Dictionary<string, Thread> _threadDict = new Dictionary<string, Thread>(); 
        private Dictionary<string, MessageThread> _myThreadDict = new Dictionary<string, MessageThread>(); 
        public void Run()
        {
            _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, Port);

            try
            {
                _serverSocket.Bind(endPoint);
                _serverSocket.Listen(5);

                _listenThread = new Thread(ListenConnection);
                _listenThread.IsBackground = true;
                _listenThread.Start();

                log.Info("Start Listen Incoming Connection");

                InitSendThread();

            }
            catch (Exception ex)
            {
                log.Error("Server Socket Exception", ex);
            }
        }

        private void ListenConnection()
        {
            while (true)
            {
                Socket incomingSocket = _serverSocket.Accept();
                //incomingSocket.Send(Encoding.ASCII.GetBytes("Server Says Hello"));

                log.InfoFormat("[{0}] Connected.", incomingSocket.RemoteEndPoint);

                MessageThread mThread = new MessageThread(incomingSocket);
                _myThreadDict.Add(incomingSocket.RemoteEndPoint.ToString(), mThread);
                mThread.Start();

                //Thread thread = new Thread(ReceiveMessage);
                //thread.IsBackground = true;
                //_threadDict.Add(incomingSocket.RemoteEndPoint.ToString(), thread);
                //thread.Start(incomingSocket);
            }
        }

        private void ReceiveMessage(object incomingSocket)
        {
            Socket socket = incomingSocket as Socket;
            
            if (socket == null)
            {
                Console.WriteLine("***ReceiveMessage but socket is null");
                return;
            }

            string remoteAddress = socket.RemoteEndPoint.ToString();
            byte[] buffer = new byte[100 * 1024];

            while (true)
            {
                try
                {
                    var length = socket.Receive(buffer);
                    if (length == 0)
                    {
                        log.WarnFormat("ReceiveMessage 0 byte from client, close connection between [{0}].", remoteAddress);   
                        //socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                        _threadDict.Remove(remoteAddress);
                        break;
                    }
                    Console.WriteLine("{0}, [{1}] Send: [{2}]", UtcNow, socket.RemoteEndPoint, Encoding.ASCII.GetString(buffer, 0, length));
                    socket.Send(Encoding.ASCII.GetBytes(UtcNow));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Receive Message Exception, " + ex.Message);
                    //socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                    break;
                }
            }
        }

        private void InitSendThread()
        {
            _sendThread = new Thread(SendMessage);
            _sendThread.IsBackground = true;
            _sendThread.Start();
        }

        private void SendMessage()
        {
            log.Info("Start Send Message Thread");

            while (true)
            {
                string message = Console.ReadLine();

                if (message == String.Empty)
                {
                    continue;
                }

                foreach (var key in _myThreadDict.Keys)
                {
                    _myThreadDict[key].Send(message);
                }
            }
        }

        private string UtcNow => DateTime.UtcNow.ToString("O");
    }

    class MessageThread
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly Socket _socket;
        private readonly Thread _thread;

        private int _totalBytes = 0;

        public MessageThread(Socket socket)
        {
            _socket = socket;
            _thread = new Thread(ReceiveMessage);
            _thread.IsBackground = true;            
        }

        public void Start()
        {
            _thread.Start();
        }

        public void Send(string message)
        {
            _socket.Send(Encoding.ASCII.GetBytes(message));
        }

        public int TotalBytes => _totalBytes;

        private void ReceiveMessage()
        {
            if (_socket == null)
            {
                log.Warn("***ReceiveMessage but _socket is null");
                return;
            }

            string remoteAddress = _socket.RemoteEndPoint.ToString();

            while (true)
            {
                try
                {
                    byte[] buffer = new byte[1024];

                    var length = _socket.Receive(buffer);
                    if (length == 0)
                    {
                        log.WarnFormat("ReceiveMessage 0 byte from client, close connection between [{0}].",
                            remoteAddress);
                        //_socket.Shutdown(SocketShutdown.Both);
                        _socket.Close();
                        //_threadDict.Remove(remoteAddress);
                        break;
                    }

                    _totalBytes += length;
                    log.InfoFormat("**[{0}]: {1} ", _socket.RemoteEndPoint, _totalBytes);

                    if (buffer[0] == 36) // 36 is $
                    {
                        log.DebugFormat("[{0}] Send: [{1}], [{2}]", _socket.RemoteEndPoint, ProcessDoller(buffer, length), ConvertToString(buffer.Take(length)));
                    }
                    else
                    {
                        string message = Encoding.ASCII.GetString(buffer, 0, length);
                        log.DebugFormat("[{0}] Send: [{1}]", _socket.RemoteEndPoint, Encoding.ASCII.GetString(buffer, 0, length));
                        if (message != "OK! Monitor Mode!")
                        {
                            Send(UtcNow);
                        }
                    }

                    //_socket.Send(Encoding.ASCII.GetBytes(UtcNow));
                }
                catch (Exception ex)
                {
                    log.Info("Receive Message Exception", ex);
                    //_socket.Shutdown(SocketShutdown.Both);
                    _socket.Close();
                    break;
                }
            }
        }

        private string UtcNow => DateTime.UtcNow.ToString("O");

        private string ProcessDoller(byte[] buffer, int length)
        {
            StringBuilder sb = new StringBuilder();

            // head
            sb.Append(Handle(buffer, 0x00, 0x00) + ",");
            // imei
            sb.Append(Handle(buffer, 0x01, 0x05) + ",");
            // time
            sb.Append(Handle(buffer, 0x06, 0x08) + ",");
            // date
            sb.Append(Handle(buffer, 0x09, 0x0B) + ",");
            // lat
            sb.Append(Handle(buffer, 0x0C, 0x0F) + ",");
            // reverser
            sb.Append(Handle(buffer, 0x10, 0x11) + ",");
            // lng N E AV
            sb.Append(Handle(buffer, 0x11, 0x15) + ",");
            // speed dir
            sb.Append(Handle(buffer, 0x16, 0x18) + ",");
            // vehicle status
            sb.Append(Handle(buffer, 0x19, 0x1C) + ",");
            // user alarm flag
            sb.Append(Handle(buffer, 0x1D, 0x1D) + ",");
            // reverser
            sb.Append(Handle(buffer, 0x1E, 0x1E) + ",");
            // record num
            sb.Append(Handle(buffer, 0x1F, 0x1F) + ",");
            // reverser ???
            sb.Append(Handle(buffer, 0x20, length));

            return sb.ToString();
        }

        private string Handle(byte[] buffer, int start, int end)
        {
            return buffer.Skip(start)
                .Take(end - start + 1)
                .Select(a => a.ToString("x2"))
                .Aggregate((a, b) => a + b);
        }

        private string ConvertToString<T>(IEnumerable<T> array)
        {
            return array.Select(a => a.ToString()).ToList().Aggregate((a, b) => a + "," + b);
        }

    }


}
