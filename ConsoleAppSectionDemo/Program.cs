﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ConsoleAppSectionDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            new SectionReader().Read("socketServer");

            Console.WriteLine("press any key to continue.....");
            Console.ReadKey();
        }
    }

    class SectionReader
    {
        public void Read(string sectionName)
        {
            var obj = ConfigurationManager.GetSection(sectionName) as SocketServerConfig;
            int a = 1;
        }
    }

    class SocketServerConfig : ConfigurationSection
    {
        [ConfigurationProperty("servers", IsRequired = true)]
        public ServerCollection Servers => this["servers"] as ServerCollection;
    }

    [ConfigurationCollection(typeof (Server), AddItemName = "server")]
    class ServerCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Server();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as Server).Name;
        }

        public Server this[int i] => BaseGet(i) as Server;
    }

    class Server : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name => this["name"] as string;

        [ConfigurationProperty("port", IsRequired = true)]
        public string Port => this["port"] as string;
    }   
}