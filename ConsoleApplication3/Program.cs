﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {

            string abc = @"Checking NecklaceLogQueue, 15
Checking NecklaceLightQueue, 21
Checking NecklaceWiFiQueue, 230
Checking NecklaceGSMStatusQueue, 51
Checking NecklaceGPSStatusQueue, 0
Checking NecklaceDebugStatusQueue, 0
Checking NecklaceCycleStatusQueue, 0
Checking NecklaceAccelerometerQueue, 150
Checking NecklaceLocationQueue, 10
Checking NecklaceBatteryQueue, 50";

            Regex regex = new Regex(@"\d{1,}");

            var ms = regex.Matches(abc);
            foreach (var m in ms)
            {
                Console.WriteLine(int.Parse(m.ToString()) > 50);
            }
            Console.WriteLine(ms.Count);
            Console.ReadKey();
        }
    }
}
