﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {

            //List<Task> tasks = new List<Task>();


            //for (int i = 0; i < 100; i++)
            //{
            //    tasks.Add(new Task(() =>
            //    {
            //        new SocketClient().Run();
            //    }));
            //}

            //tasks.ForEach(t=>t.Start());

            new SocketClient().Run();
            Console.ReadLine();
            Console.WriteLine("press any key to exit....");
            Console.ReadKey();
        }
    }

    interface IRun
    {
        void Run();
    }
    class SocketClient : IRun
    {
        private static int Port = 33333;
        private static string Host = "192.168.1.118";
        private Socket _clientSocket;

        private byte[] _buffer = new byte[100 * 1024];
        private Thread _thread;
        private bool _running = true;

        public void Run()
        {
            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(Host), Port);
            _clientSocket.Connect(endPoint);

            _thread = new Thread(ReceiveMessage);
            _thread.IsBackground = true;
            _thread.Start();

            Console.WriteLine("type words to server");
            while (true)
            {
                string message = Console.ReadLine();
                if (message == null) continue;
                
                _clientSocket.Send(Encoding.ASCII.GetBytes(message));

                if (message == "quit")
                {
                    _running = false;
                    break;
                }
            }
        }

        private void ReceiveMessage()
        {
            while (_running)
            {
                try
                {
                    int length = _clientSocket.Receive(_buffer);
                    Console.WriteLine("receive [{0}] from server", Encoding.ASCII.GetString(_buffer, 0, length));
                }
                catch (Exception ex)
                {
                    break;
                }
            }

            _clientSocket.Close();
            Console.WriteLine("exit ReceiveMessage()");
        }
    }
}
